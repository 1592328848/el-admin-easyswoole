<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/2/1 13:03
 */
declare(strict_types=1);

namespace App\Model;

class SysDeptModel extends BaseModel
{
    protected $tableName = 'sys_dept';
}
