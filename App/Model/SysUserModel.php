<?php
declare(strict_types=1);

/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\Model;

/**
 * Class SysUserModel
 *
 * @package App\Model
 * @property string $username
 * @property string $password
 */
class SysUserModel extends BaseModel
{
    protected $tableName = 'sys_user';

    public function dept()
    {
        return $this->hasOne(SysDeptModel::class, null, 'dept_id', 'dept_id');
    }

    public function getIsAdminAttr($value)
    {
        return boolval(ord($value));
    }

    public function getEnabledAttr($value)
    {
        return boolval(ord($value));
    }
}
