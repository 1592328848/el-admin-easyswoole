<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/1/30 14:40
 */
declare(strict_types=1);

namespace App\Utility\Rsa;

class RsaKeyPair
{
    private $publicKey;
    private $privateKey;

    public function __construct(string $publicKey, string $privateKey)
    {
        $this->publicKey  = $publicKey;
        $this->privateKey = $privateKey;
    }

    public function getPublicKey()
    {
        return $this->publicKey;
    }

    public function getPrivateKey()
    {
        return $this->privateKey;
    }
}
