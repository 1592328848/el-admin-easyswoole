<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/1/30 12:41
 */
declare(strict_types=1);

namespace App\Utility\Jwt;

use App\Utility\IdUtil;
use EasySwoole\Jwt\Jwt;

class JwtUtil
{
    public static function createToken(string $username)
    {
        $jwtConfig = config('jwt');

        $jti = IdUtil::simpleUUID();

        $jwtObject = Jwt::getInstance()->setSecretKey($jwtConfig['base64-secret'])->publish();

        $jwtObject->setPrefix($jwtConfig['token-start-with']);
        $jwtObject->setAlg($jwtConfig['alg']);
        $jwtObject->setAud($jwtConfig['iss']);                                          // 用户
        $jwtObject->setExp(time() + $jwtConfig['token-validity-in-seconds']);      // 过期时间
        $jwtObject->setIat(time());                                                     // 发布时间
        $jwtObject->setIss($jwtConfig['iss']);                                          // 发行人
        $jwtObject->setJti($jti);                                                       // jwt id 用于标识该jwt
        $jwtObject->setSub($username);                                                  // 主题
        // 自定义数据
        $jwtObject->setData(['user' => $username]);
        // 最终生成的token
        return $jwtObject->__toString();
    }
}
