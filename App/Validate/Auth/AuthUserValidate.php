<?php
declare(strict_types=1);
/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\Validate\Auth;

use EasyApi\Validate\Validate;

class AuthUserValidate extends Validate
{
    protected $rule = [
        'username' => 'require',
        'password' => 'require',
        'code'     => 'require',
        'uuid'     => 'require',
    ];
}
