<?php
declare(strict_types=1);
/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\Router\Admin;

use EasySwooleApi\Core\Router\IRouterInterface;
use FastRoute\RouteCollector;

class AuthRouter implements IRouterInterface
{
    public function register(RouteCollector &$routeCollector): void
    {
        $routeCollector->addGroup('/auth', function (RouteCollector $routeCollector) {
            $routeCollector->addRoute('GET', '/code', '/Admin/Auth/code');
            $routeCollector->addRoute('GET', '/info', '/Admin/Auth/info');
            $routeCollector->addRoute('POST', '/login', '/Admin/Auth/login');
            $routeCollector->addRoute('DELETE', '/logout', '/Admin/Auth/logout');
        });
    }
}
