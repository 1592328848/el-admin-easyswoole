<?php
declare(strict_types=1);
/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\Router\Admin\Api;

use EasySwooleApi\Core\Router\IRouterInterface;
use FastRoute\RouteCollector;

class MenusRouter implements IRouterInterface
{
    public function register(RouteCollector &$routeCollector): void
    {
        $routeCollector->addGroup('/api', function (RouteCollector $routeCollector) {
            $routeCollector->addGroup('/menus', function (RouteCollector $routeCollector) {
                $routeCollector->addRoute('GET', '/build', '/Admin/Api/Menus/build');
            });
        });
    }
}
