<?php
declare(strict_types=1);
/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\Service;

use App\Dao\SysUserDao;
use App\Model\SysUserModel;

class SysUserService extends BaseService
{
    /**
     * Note:     [findByUsername Description]
     * Author:   longhui.huang <1592328848@qq.com>
     * DateTime: 2024/2/1 12:59
     *
     * @param string $username
     *
     * @return SysUserModel|\App\Model\BaseModel|array|bool|\EasySwoole\ORM\AbstractModel|\EasySwoole\ORM\Db\Cursor|\EasySwoole\ORM\Db\CursorInterface|null
     * @throws \EasySwoole\Mysqli\Exception\Exception
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    public function findByUsername(string $username)
    {
        /** @var SysUserDao $userDaoObj */
        $userDaoObj = container()->make(SysUserDao::class);
        return $userDaoObj->getOne(['username' => $username]);
    }
}
