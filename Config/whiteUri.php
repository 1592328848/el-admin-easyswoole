<?php
declare(strict_types=1);
return [
    'no_need_login' => [
        '/auth/code',
        '/auth/login',
    ],
    'no_need_right' => [],
];
